const nodeExternals = require('webpack-node-externals');
const slsw = require('serverless-webpack');
const env = process.env.NODE_ENV;
process.env.WEBPACK_MODE === env || 'development';
module.exports = {
  entry: slsw.lib.entries,
  target: 'node',
  mode: 'development',
  externals: [nodeExternals()],
  optimization: {
    minimize: true
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: __dirname,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx']
  }
};
