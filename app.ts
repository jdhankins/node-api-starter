import config from './src/config';
import express from 'express';
import cors from 'cors';
import serverless from 'serverless-http';
import bodyParser from 'body-parser';
import routes from './src/routes';
import logger from './src/loaders/logger';
import pe from 'parse-error';

const app = express();

app.get('/status', (req, res) => {
  res.status(200).end();
});
app.head('/status', (req, res) => {
  res.status(200).end();
});

app.enable('trust proxy');
app.use(
  cors({
    exposedHeaders: ['content-type', 'authorization'],
    allowedHeaders: ['content-type', 'authorization']
  })
);
app.use(bodyParser.json());
app.use(config.api.prefix, routes());

app.use('/', function(req, res) {
  res.statusCode = 400; //send the appropriate status code
  res.json({ status: 'error', message: 'root API', data: {} });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err: any = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err: any, req: any, res: any) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// export default app;
export const handler = serverless(app);
export default app;

//This is here to handle all the uncaught promise rejections
process.on('unhandledRejection', error => {
  logger.error('app.js', 'Uncaught Error', pe(error));
});

logger.silly('Root directory: %s', __dirname);
