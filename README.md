# node-api-starter


### Operating Instructions

#### Dependencies

yarn: [Instructions](https://yarnpkg.com/en/docs/install)

serverless:  [Instructions](https://serverless.com/framework/docs/providers/aws/guide/installation)

##### To Run Local:
`yarn install` from project root.  This will install all local dependencies.

`yarn start` will start the api on port identified in the .env file in the root.

##### Deploy Serverless:
`yarn deploy:stage`will deploy as aws lambda.  (proper permissions need to be applied)

##### Get System Status (up status):
`GET /status` 200 response will confirm api is running.
