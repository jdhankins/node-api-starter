import jwt from 'jsonwebtoken';
import config from '../config';
import logger from '../loaders/logger';
import { returnError, returnSuccess } from '../controllers/return';
import { HEADER_TOKEN, ROLE } from '../config/constants';

import atob from 'atob';

export const testReq = (req, res) => {
  const { headers } = req;
  return returnSuccess(res, { headers });
};

/**
 * Validates Api Key against header and value stored as env variable.
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
export const checkApiKey = (req, res, next) => {
  logger.debug('auth.middleware:checkApiKey:req.headers: %o', req.headers);
  const apiKey = req.headers[HEADER_TOKEN.API_KEY];
  if (!apiKey) {
    return returnError(res, 'Missing Api Key.');
  } else {
    if (apiKey !== config.apiKey) {
      return returnError(res, 'Invalid Api Key.');
    }
  }
  next();
};

const decodeAuthorizationHeader = authorizationHeader => {
  try {
    let decoded = atob(authorizationHeader);
    decoded = decoded.split(':');
    const id = decoded[0];
    const key = decoded[1];
    return { id, key };
  } catch (e) {
    return null;
  }
};

export const AUTHORIZATION_TYPE = {
  TOKEN: 'TOKEN',
  BASIC: 'BASIC',
  BEARER: 'BEARER'
};

/**
 * Gets token from authorization header set as Token or Bearer
 * @param req
 * @returns {string|null}
 */
const getAuthTokenFromHeader = req => {
  const {
    headers: { authorization }
  } = req;

  if (authorization) {
    return req.headers.authorization.split(' ')[1];
  }
  return null;
};

export const isBasicAuth = req => {
  const {
    headers: { authorization }
  } = req;

  if (authorization) {
    return authorization.split(' ')[0].toUpperCase() === AUTHORIZATION_TYPE.BASIC;
  }
  return null;
};

/**
 * Validates JWT token passed in the authorization header
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
export const checkAuthorization = (req, res, next) => {
  logger.debug('auth.middleware:checkAuthToken:req.headers: %O', req.headers);
  const token = getAuthTokenFromHeader(req);
  if (!token) return returnError(res, 'Missing Authorization.', 403);
  if (isBasicAuth(req)) return next();
  jwt.verify(token, config.jwtSecret, (err, decoded) => {
    if (err) return returnError(res, err, 500);
    req.body.authorizedUser = decoded;
    logger.silly('auth.middleware:checkAuthToken:authorizedUser(in req.body): %O', req.body.authorizedUser);
    next();
  });
};

/**
 * Next four methods check of the requester has the correct role.
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
export const checkRoleSuperAdmin = (req, res, next) => {
  logger.debug('auth.middleware:checkRoleSuperAdmin:req.body.authorizedUser:role: %o', req.body.authorizedUser.role);
  const {
    user: { role }
  } = req.body.authorizedUser;
  if (role && role.toUpperCase() !== ROLE.SUPER_ADMIN) return returnError(res, 'Inadequate permissions.', 403);
  next();
};

export const checkRoleAdmin = (req, res, next) => {
  logger.debug('auth.middleware:checkRoleAdmin:req.body.authorizedUser:role: %o', req.body.authorizedUser);
  const {
    user: { role }
  } = req.body.authorizedUser;
  if (role && role.toUpperCase() !== ROLE.ADMIN) return returnError(res, 'Inadequate permissions.', 403);
  next();
};

export const attachCurrentUser = async (req, res, next) => {
  logger.debug('auth.middleware:attachCurrentUser', req.body.authorizedUser);
  return returnSuccess(res, req.body.authorizedUser);
};
