import { to } from 'await-to-js';

export const execute = async promise => {
  const [error, response]: any = await to(promise);
  if (error) {
    let err;
    if (error.response) {
      const { data } = error.response;
      err = data;
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // console.log(error.response.data);
      // console.log(error.response.status);
      // console.log(error.response.headers);

      // this block handles errors from express-validator default response message error array
      const { message } = data;
      if (message) {
        const { errors } = message;
        if (errors) err = `${errors[0].param}: ${errors[0].msg}`; // grab the 1st message only to not overload any messaging.
      }
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
      err = error.request;
    } else {
      // Something happened in setting up the request that triggered an Error
      if (error.message) {
        err = error.message;
      } else {
        err.message = err;
      }
    }
    return [err, null];
  }

  return [null, response];
};
