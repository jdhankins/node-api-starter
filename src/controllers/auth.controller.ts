import { returnError, returnSuccess } from './return';
import { execute } from '../async/util';
import logger from '../loaders/logger';
import { login } from '../services/auth.service';
import { ROLE } from '../config/constants';

export const loginRequest = async (req, res) => {
  logger.debug('auth.controller:loginRequest: %O', req.body.email);
  const [err, response] = await execute(login(req.body, ROLE.ADMIN));
  if (err) return returnError(res, err, 422);
  return returnSuccess(res, response);
};
