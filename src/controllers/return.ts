import camelcaseKeys from 'camelcase-keys';

export const returnError = (res, err, code = 500) => {
  // Error Web Response
  let error = {
    message: ''
  };
  if (typeof err === 'object' && typeof err.message !== 'undefined') {
    error.message = err.message;
  } else {
    error.message = err;
  }
  if (typeof code !== 'undefined') res.statusCode = code;
  return res.json(camelcaseKeys(error, { deep: true }));
};

export const returnTransactionError = (res, err, code = 400) => {
  res.statusCode = code;
  return res.json(camelcaseKeys(err, { deep: true }));
};

export const returnSuccess = (res, data, code = 200) => {
  // Success Web Response
  let send_data = {};
  if (typeof data === 'object') {
    send_data = Object.assign(data, send_data); //merge the objects
  }
  if (typeof code !== 'undefined') res.statusCode = code;
  res.setHeader('Expires', '-1');
  res.setHeader('Cache-Control', 'no-cache');
  return res.json(camelcaseKeys(send_data, { deep: true }));
};
