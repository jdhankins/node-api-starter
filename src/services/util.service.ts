import CONFIG from '../config';
import moment from 'moment';

export const stringJSONValues = obj => {
  for (var k in obj) {
    if (obj.hasOwnProperty(k)) {
      obj[k] = String(obj[k]);
    }
  }
  return obj;
};

export const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

export const convertObjectArrayToCSV = (objArray, fields) => {
  const Json2csvParser = require('json2csv').Parser;
  const json2csvParser = new Json2csvParser({ fields });
  return json2csvParser.parse(objArray);
};

export const isEmptyObject = obj => {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
};

export const isDevelopment = () => {
  return CONFIG.stage === 'development';
};

export const getThroughPut = () => {
  return isDevelopment() ? 5 : 'ON_DEMAND';
};

export const addOrUpdateArray = (array, object) => {
  const index = array.findIndex(obj => obj.id === object.id);
  if (index === -1) {
    array.push(object);
  } else {
    array[index] = object;
  }
};

export const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const logMinutesPast = start => {
  const endTime = new Date();
  return moment.duration(moment(endTime, 'YYYY/MM/DD HH:mm').diff(moment(start, 'YYYY/MM/DD HH:mm'))).asMinutes();
};
