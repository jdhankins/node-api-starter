import jwt from 'jsonwebtoken';
import config from '../config';
import logger from '../loaders/logger';
import * as bcrypt from 'bcryptjs';
import { execute } from '../async/util';
import { ROLE } from '../config/constants';

/**
 * login
 * @param loginObj
 * @param roleType
 * @returns {Promise<{user: null, token: {}}>}
 */
export const login = async (loginObj, roleType) => {
  const { email: login, password } = loginObj;
  logger.silly('auth.service:login: %O ', loginObj);

  let user = null;
  let token: any = {};

  switch (roleType) {
    case ROLE.SUPER_ADMIN:
      user = {};
      break;
    case ROLE.ADMIN:
      user = {};
      break;
  }
  if (!user) {
    throw new Error('User not registered');
  }

  // todo: remove this
  const hashedPassword = await bcrypt.hashSync(password, parseInt(config.saltRounds));

  const [bcryptError, validPassword] = await execute(bcrypt.compare(password, hashedPassword));
  if (validPassword) {
    logger.silly('auth.service:login: Password is valid, generating JTW');

    removeSecureFields(user);
    switch (roleType) {
      case ROLE.ADMIN:
        // no additional fields
        token.user = user;
        token = generateToken(token);

        break;
      case ROLE.SUPER_ADMIN:
        token.user = {
          email: user.email,
          role: user.role
        };
        token = generateToken(token);
        break;
    }

    return { user, token };
  } else {
    logger.silly('auth.service:login: Password is invalid. %O', bcryptError);

    throw new Error('Invalid Password');
  }
};

/**
 * Generates a jwt token for any user type
 * @param tokenBody
 */
export const generateToken = tokenBody => {
  const token = JSON.parse(JSON.stringify(tokenBody));

  logger.silly('auth.service:generateToken: Sign JWT for authorizedUser: %O', token);
  return jwt.sign(JSON.parse(JSON.stringify(token)), config.jwtSecret, {
    expiresIn: config.jwtExpiration
  });
};

const removeSecureFields = obj => {
  Reflect.deleteProperty(obj, 'password');
  Reflect.deleteProperty(obj, 'salt');
};
