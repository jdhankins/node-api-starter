import adminRoutes from './admin.routes';
import { Router } from 'express';

export default () => {
  const app = Router();
  adminRoutes(app);
  return app;
};
