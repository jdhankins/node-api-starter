import { Router } from 'express';
import { loginRequest } from '../controllers/auth.controller';
import isValidRequest from './validation';
import { loginValidator } from './validation/loginValidation';
import { checkApiKey } from '../middleware/auth.middleware';

const router = Router();

export default app => {
  app.use('/admin', router);
  router.post('/login', checkApiKey, loginValidator, isValidRequest, loginRequest);
};
