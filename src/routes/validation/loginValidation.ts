import { check } from 'express-validator';

export const loginValidator = [
  check('email')
    .isEmail()
    .withMessage('Invalid email address.')
    .normalizeEmail(),
  check('password')
    .not()
    .isEmpty()
    .withMessage('This field is required.')
];
