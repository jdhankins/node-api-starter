import { validationResult } from 'express-validator';
import { returnError } from '../../controllers/return';
import logger from '../../loaders/logger';

/**
 * This method works in harmony with the express-validator class validators
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
const isValidRequest = (req, res, next) => {
  const errors = validationResult(req);
  logger.silly('routes.validation:isValidRequestBody: %O', errors.isEmpty());

  if (!errors.isEmpty()) {
    return returnError(res, errors, 422);
  }
  next();
};

export default isValidRequest;
