export const ROLE = {
  SUPER_ADMIN: 'SUPER_ADMIN',
  ADMIN: 'ADMIN'
};

export const HEADER_TOKEN = {
  API_KEY: 'x-api-key',
  AUTH_TOKEN: 'token',
  AUTHORIZATION: 'authorization'
};

export const ADMIN_ROLES = [ROLE.ADMIN, ROLE.SUPER_ADMIN];
export const SUPER_ADMIN_ROLES = [ROLE.SUPER_ADMIN];