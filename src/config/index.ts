import dotenv from 'dotenv';

const envFound = dotenv.config();
if (!envFound) {
  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

const {
  PORT,
  STAGE,
  API_PREFIX,
  JWT_SECRET_KEY,
  JWT_EXPIRATION,
  SALT_ROUNDS,
  LOG_LEVEL,
  ADMIN_EMAIL,
  ADMIN_PASSWORD,
  API_KEY
} = process.env;

export default {
  port: parseInt(PORT, 10),
  env: process.env.NODE_ENV || 'development',
  stage: STAGE,
  jwtSecret: JWT_SECRET_KEY,
  jwtExpiration: JWT_EXPIRATION,
  saltRounds: SALT_ROUNDS,
  logs: {
    level: LOG_LEVEL || 'silly'
  },
  api: {
    prefix: API_PREFIX
  },
  adminEmail: ADMIN_EMAIL,
  adminPassword: ADMIN_PASSWORD,
  apiKey: API_KEY
};
