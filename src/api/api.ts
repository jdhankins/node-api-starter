import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.put['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';

axios.interceptors.request.use(cfg => {
  const config = cfg;
  config.headers['Authorization'] = 'value';
  return config;
});

export const api = axios;
