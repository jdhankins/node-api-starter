module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: ['prettier'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    'comma-dangle': [
      'error',
      {
        arrays: 'never',
        objects: 'never',
        imports: 'never',
        exports: 'never',
        functions: 'never'
      }
    ],
    'no-plusplus': 'off',
    'consistent-return': 'off',
    'import/no-cycle': 'off',
    'no-use-before-define': 'off',
    'no-param-reassign': 'off',
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        printWidth: 200
      }
    ]
  },
  plugins: ['prettier']
};
